<?php

/**
 * @file
 * uw_slack.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_slack_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'slack_customize_icon';
  $strongarm->value = 'none';
  $export['slack_customize_icon'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'slack_icon_emoji';
  $strongarm->value = '';
  $export['slack_icon_emoji'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'slack_icon_type';
  $strongarm->value = 'image';
  $export['slack_icon_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'slack_icon_url';
  $strongarm->value = 'https://wms-aux1.uwaterloo.ca/bots/wcms_emoji.png';
  $export['slack_icon_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'slack_username';
  $strongarm->value = 'WCMS';
  $export['slack_username'] = $strongarm;

  return $export;
}
