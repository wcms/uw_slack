<?php

/**
 * @file
 * uw_slack.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_slack_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
