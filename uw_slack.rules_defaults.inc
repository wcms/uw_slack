<?php

/**
 * @file
 * uw_slack.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_slack_default_rules_configuration() {
  $items = array();
  $items['rules_tell_slack_about_form_submission'] = entity_import('rules_config', '{ "rules_tell_slack_about_form_submission" : {
      "LABEL" : "Tell Slack about form submission",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Slack" ],
      "REQUIRES" : [ "rules_condition_site_variable", "rules", "slack", "webform_rules" ],
      "ON" : { "webform_rules_submit" : [] },
      "IF" : [
        { "rules_condition_site_variable_compare_variable" : {
            "variable_name" : "uw_slack_webform",
            "variable_value" : "1",
            "loosely_comparison" : "1"
          }
        }
      ],
      "DO" : [
        { "slack_send_message" : { "message" : ":scroll: [node:url] just received a \\u003C[site:url]node\\/[node:nid]\\/submission\\/[data:sid]|web form submission\\u003E. " } }
      ]
    }
  }');
  $items['rules_tell_teams_about_form_submission'] = entity_import('rules_config', '{ "rules_tell_teams_about_form_submission" : {
      "LABEL" : "Tell MS Teams about form submission",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "teams" ],
      "REQUIRES" : [
        "rules_condition_site_variable",
        "rules",
        "rules_teams",
        "webform_rules"
      ],
      "ON" : { "webform_rules_submit" : [] },
      "IF" : [
        { "rules_condition_site_variable_compare_variable" : {
            "variable_name" : "uw_slack_teams_webform",
            "variable_value" : "1",
            "loosely_comparison" : "1"
          }
        }
      ],
      "DO" : [
        { "rules_teams_send_message" : {
            "message" : "[[node:url]]([node:url]) just received a [web form submission]([site:url]node\\/[node:nid]\\/submission\\/[data:sid]).",
            "color" : "D2B48C"
          }
        }
      ]
    }
  }');
  $items['rules_uw_slack_needs_review'] = entity_import('rules_config', '{ "rules_uw_slack_needs_review" : {
      "LABEL" : "Tell Slack about \\u0022needs review\\u0022",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Slack" ],
      "REQUIRES" : [
        "workbench_moderation",
        "rules_condition_site_variable",
        "rules",
        "slack"
      ],
      "ON" : { "workbench_moderation_after_moderation_transition" : [] },
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "needs_review" } },
        { "rules_condition_site_variable_compare_variable" : {
            "variable_name" : "uw_slack_needs_review",
            "variable_value" : "1",
            "loosely_comparison" : "1"
          }
        }
      ],
      "DO" : [
        { "slack_send_message" : { "message" : ":orange_book: [node:url] was just marked as \\u0022needs review\\u0022." } }
      ]
    }
  }');
  $items['rules_uw_slack_published'] = entity_import('rules_config', '{ "rules_uw_slack_published" : {
      "LABEL" : "Tell Slack about \\u0022published\\u0022",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Slack" ],
      "REQUIRES" : [
        "workbench_moderation",
        "rules_condition_site_variable",
        "rules",
        "slack"
      ],
      "ON" : { "workbench_moderation_after_moderation_transition" : [] },
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "published" } },
        { "rules_condition_site_variable_compare_variable" : {
            "variable_name" : "uw_slack_published",
            "variable_value" : "1",
            "loosely_comparison" : "1"
          }
        }
      ],
      "DO" : [
        { "slack_send_message" : { "message" : ":green_book: [node:url] was just marked as \\u0022published\\u0022." } }
      ]
    }
  }');
  $items['rules_uw_slack_teams_needs_review'] = entity_import('rules_config', '{ "rules_uw_slack_teams_needs_review" : {
      "LABEL" : "Tell MS Teams about \\u0022needs review\\u0022 content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "teams" ],
      "REQUIRES" : [
        "workbench_moderation",
        "rules_condition_site_variable",
        "rules",
        "rules_teams"
      ],
      "ON" : { "workbench_moderation_after_moderation_transition" : [] },
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "needs_review" } },
        { "rules_condition_site_variable_compare_variable" : {
            "variable_name" : "uw_slack_teams_needs_review",
            "variable_value" : "1",
            "loosely_comparison" : "1"
          }
        }
      ],
      "DO" : [
        { "rules_teams_send_message" : {
            "message" : "[[node:url]]([node:url]) was just marked as \\u0022needs review\\u0022.",
            "color" : "FFA500"
          }
        }
      ]
    }
  }');
  $items['rules_uw_slack_teams_published'] = entity_import('rules_config', '{ "rules_uw_slack_teams_published" : {
      "LABEL" : "Tell MS Teams about \\u0022published\\u0022 content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "teams" ],
      "REQUIRES" : [
        "workbench_moderation",
        "rules_condition_site_variable",
        "rules",
        "rules_teams"
      ],
      "ON" : { "workbench_moderation_after_moderation_transition" : [] },
      "IF" : [
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "published" } },
        { "rules_condition_site_variable_compare_variable" : {
            "variable_name" : "uw_slack_teams_published",
            "variable_value" : "1",
            "loosely_comparison" : "1"
          }
        }
      ],
      "DO" : [
        { "rules_teams_send_message" : {
            "message" : "[[node:url]]([node:url]) was just marked as \\u0022published\\u0022.",
            "color" : "008000"
          }
        }
      ]
    }
  }');
  return $items;
}
